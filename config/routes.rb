Rails.application.routes.draw do
	devise_for :users, controllers: {registrations: 'edit_profile',
		confirmations: 'confirmation'}
	
	get 'main_groups/manage'
	patch 'main_groups/edit'
	
	get 'subgroups/manage'
	patch 'subgroups/edit'
	
	get 'subgroups/show_users'
	patch 'subgroups/edit_users'
	
	get 'acolyte/index'
	get 'acolyte/user_offer_self_work'
	patch 'acolyte/save_dispositions'
	get 'acolyte/show_schedule'
	post 'acolyte/generate_schedule'
	get 'acolyte/edit_schedule'
	post 'acolyte/save_schedule'
	get 'acolyte/select_date_ranking'
	get 'acolyte/show_ranking'
	get 'acolyte/write_news'
	post 'acolyte/create_news'
	get 'acolyte/show_news'
	delete 'acolyte/delete_news'
	
	get 'calendar/show'
	get 'calendar/show_event'
	get 'calendar/add_event'
	post 'calendar/create_event'
	get 'calendar/show_event_to_edit'
	patch 'calendar/edit_event'
	delete 'calendar/destroy_event'
	delete 'calendar/destroy_event_series'
	get 'calendar/event_categories'
	patch 'calendar/event_category_edit'
	get 'calendar/show_attendance_list'
	get 'calendar/edit_attendance_list'
	get 'calendar/edit_mandatory_attendance_list'
	post 'calendar/save_attendance_list'
	post 'calendar/save_mandatory_attendance_list'
	get 'calendar/edit_points'
	post 'calendar/save_points'
	
	root 'home#index'

	# The priority is based upon order of creation: first created -> highest priority.
	# See how all your routes lay out with "rake routes".

	# You can have the root of your site routed with "root"
	# root 'welcome#index'

	# Example of regular route:
	#   get 'products/:id' => 'catalog#view'

	# Example of named route that can be invoked with purchase_url(id: product.id)
	#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

	# Example resource route (maps HTTP verbs to controller actions automatically):
	#   resources :products

	# Example resource route with options:
	#   resources :products do
	#     member do
	#       get 'short'
	#       post 'toggle'
	#     end
	#
	#     collection do
	#       get 'sold'
	#     end
	#   end

	# Example resource route with sub-resources:
	#   resources :products do
	#     resources :comments, :sales
	#     resource :seller
	#   end

	# Example resource route with more complex sub-resources:
	#   resources :products do
	#     resources :comments
	#     resources :sales do
	#       get 'recent', on: :collection
	#     end
	#   end

	# Example resource route with concerns:
	#   concern :toggleable do
	#     post 'toggle'
	#   end
	#   resources :posts, concerns: :toggleable
	#   resources :photos, concerns: :toggleable

	# Example resource route within a namespace:
	#   namespace :admin do
	#     # Directs /admin/products/* to Admin::ProductsController
	#     # (app/controllers/admin/products_controller.rb)
	#     resources :products
	#   end
end
