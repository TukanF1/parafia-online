class WorkPlan < ActiveRecord::Base
	belongs_to :user
	belongs_to :main_group
	has_many :tasks, dependent: :destroy
end
