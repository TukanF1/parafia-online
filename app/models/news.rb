class News < ActiveRecord::Base
	belongs_to :author, class_name: "User"
	belongs_to :main_group
	has_many :comments
end
