class MainGroup < ActiveRecord::Base
    has_many :subgroups, dependent: :destroy
    has_many :users, through: :subgroups
    has_many :event_properties
    has_many :events, through: :event_properties
    has_many :work_plans
    has_many :news
end
