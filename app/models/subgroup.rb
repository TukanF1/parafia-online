class Subgroup < ActiveRecord::Base
    belongs_to :main_group
    has_and_belongs_to_many :users
end
