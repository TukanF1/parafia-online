class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
				 :recoverable, :rememberable, :trackable, :validatable,
				 :confirmable, :lockable

	has_and_belongs_to_many :subgroups
	has_and_belongs_to_many :attendance_lists
	has_and_belongs_to_many :mandatory_attendance_lists
	has_many :main_groups, through: :subgroups
	has_many :dispositions
	has_many :work_plans
	has_many :tasks, through: :work_plans
	has_many :points
	has_many :news, foreign_key: "author_id"
	has_many :comments, foreign_key: "author_id"
	
	def short_name
		"#{first_name.first}. #{last_name}"
	end
	
	def full_name
		"#{first_name} #{last_name}"
	end
end