class Event < ActiveRecord::Base
	has_many :event_properties, dependent: :destroy
	has_many :main_groups, through: :event_properties
	has_one :attendance_list, dependent: :destroy
	has_one :mandatory_attendance_list, dependent: :destroy
	belongs_to :event_category
	has_many :related_events, class_name: "Event",
		foreign_key: "overriding_event_id"
	belongs_to :overriding_event, class_name: "Event"
	has_many :tasks, dependent: :destroy
	has_one :next_in_series_event, class_name: "Event",
		foreign_key: "previous_in_series_event_id"
	belongs_to :previous_in_series_event, class_name: "Event"
	has_many :points, dependent: :destroy
	
	def start_time_and_name
		if start_time.to_date == end_time.to_date
			"#{name}, #{I18n.l start_time, format: "%A, %d.%m.%Y, g. %H:%M"}
				- #{I18n.l end_time, format: "%H:%M"}"
		else
			"#{name}, #{I18n.l start_time, format: "%A, %d.%m.%Y, g. %H:%M"}
				- #{I18n.l end_time, format: "%A, %d.%m.%Y, g. %H:%M"}"
		end
	end
end
