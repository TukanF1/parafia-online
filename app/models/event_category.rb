class EventCategory < ActiveRecord::Base
	has_many :events
	has_many :tasks, through: :events
	has_many :dispositions
end
