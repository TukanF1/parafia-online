# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
	$(".dropdown-submenu").hover ->
		$(".dropdown-menu").each ->
			maxWidth = 0
			$(this).children().each ->
				if $(this).outerWidth(true) > maxWidth
					maxWidth = $(this).outerWidth(true)
			$(this).css("width", maxWidth + parseInt($(this).css("border-left-width")) + parseInt($(this).css("border-right-width")))
			width = $(this).outerWidth(false) - (parseInt($(this).css("border-left-width")) + parseInt($(this).css("border-right-width")))
			$(this).children().each ->
				$(this).css("width", width)
		$(".dropdown-submenu .pull-left").each ->
			width = $(this).outerWidth(true)
			$(this).children(".dropdown-menu").each ->
				width += $(this).outerWidth(true)
				$(this).css("marginLeft", "-=" + width + "px")