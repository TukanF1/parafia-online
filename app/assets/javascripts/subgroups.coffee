# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "turbolinks:load", ->
	if window.location.href.indexOf("/subgroups/manage") > 0 or
			window.location.href.indexOf("/subgroups/edit") > 0
		
		window.adminName = ""
		window.adminChecked = false
		window.moderatorName = ""
		window.moderatorChecked = false
		
		$("#selected_main_group").on "change click", (event) ->
			###$("#selected_subgroup option").remove()
			$.each($("#selected_main_group :selected"), (val, option) ->
				$("#selected_subgroup")
					.append $($("<option>", value: val).text $(option).text()).hide()
			)###
			###$.each($("#selected_subgroup option"), (val, option) ->
				if $("#selected_main_group :selected").val() == option.value
					$(option).show()
				else
					$(option).hide()
			)###
			$("#selected_subgroup option").each ->
				if $("#selected_main_group :selected").val() is this.value
					$(this).show()
				else
					$(this).hide()
		
		$("#selected_subgroup").on "change click", (event) ->
			$("#subgroup_admin").hide()
			$("#subgroup_moderator").hide()
			if window.adminName isnt ""
				$("input#" + window.adminName).prop("checked",
					window.adminChecked)
				window.adminName = ""
			if window.moderatorName isnt ""
				$("input#" + window.moderatorName).prop("checked",
					window.moderatorChecked)
				window.moderatorName = ""
			if $("#selected_subgroup_name").val() isnt ""
				$("[id='" + $("#selected_subgroup_name").val() + "_admin']").hide()
				$("[id='" + $("#selected_subgroup_name").val() + "_moderator']").hide()
			$("#subgroup_name").val $("#selected_subgroup :selected").text()
			$("#selected_subgroup_name").val $("#subgroup_name").val()
			if $("#selected_subgroup_name").val() isnt ""
				$("[id='" + $("#selected_subgroup_name").val() + "_admin']").show()
				$("[id='" + $("#selected_subgroup_name").val() + "_moderator']").show()
			$("input[name='edit']").attr disabled: true
			$("input[name='add']").attr disabled: true
			$("input[name='delete']").attr disabled: false
		
		$("#subgroup_name").on "keyup", (event) ->
			if $("#subgroup_name").val() isnt ""
				if $("#selected_subgroup :selected").text() isnt ""
					if $("#subgroup_name").val() is
							$("#selected_subgroup :selected").text()
						if window.adminName is "" and window.moderatorName is ""
							$("input[name='edit']").attr disabled: true
						else
							$("input[name='edit']").attr disabled: false
						$("input[name='add']").attr disabled: true
					else if $("#selected_subgroup option").filter(->
								$(this).text() is $("#subgroup_name").val()
							).length
						$("input[name='add']").attr disabled: true
						$("input[name='edit']").attr disabled: true
					else
						$("input[name='add']").attr disabled: false
						$("input[name='edit']").attr disabled: false
				else
					$("input[name='edit']").attr disabled: true
					if $("#selected_subgroup option").filter(->
								$(this).text() is $("#subgroup_name").val()
							).length
						$("input[name='add']").attr disabled: true
					else
						$("input[name='add']").attr disabled: false
			else
				$("input[name='add']").attr disabled: true
				$("input[name='edit']").attr disabled: true
		
		$(":checkbox").on "change", (event) ->
			if $("#subgroup_name").val() isnt "" and
					$("#selected_subgroup :selected").text() isnt ""
				$("input[name='edit']").attr disabled: false
			if $(this).attr("id").match(/admin$/)
				if window.adminName isnt $(this).attr("id")
					window.adminName = $(this).attr("id")
					window.adminChecked = !$(this).prop("checked")
				else
					window.adminName = ""
					window.adminChecked = false
					if window.moderatorName is "" and $("#subgroup_name").val() is
							$("#selected_subgroup :selected").text()
						$("input[name='edit']").attr disabled: true
			else
				if $(this).attr("id").match(/moderator$/)
					if window.moderatorName isnt $(this).attr("id")
						window.moderatorName = $(this).attr("id")
						window.moderatorChecked = !$(this).prop("checked")
					else
						window.moderatorName = ""
						window.moderatorChecked = false
						if window.adminName is "" and $("#subgroup_name").val() is
								$("#selected_subgroup :selected").text()
							$("input[name='edit']").attr disabled: true
			return true
		
		$.rails.allowAction = (link) ->
			return true unless link.attr "data-confirm"
			$.rails.showConfirmDialog link
			false
		
		$.rails.confirmed = (link) ->
			link.removeAttr "data-confirm"
			link.trigger "click.rails"
			$form.submit()
		
		$.rails.showConfirmDialog = (link) ->
			message = link.attr "data-confirm"
			html = """
				<div class="modal fade" tabindex="-1" role="dialog" id="confirmationDialog">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
						<h4 class="modal-title">Usunąć?</h4>
					  </div>
					  <div class="modal-body">
						<p>#{message} <strong>""" + $("#selected_subgroup :selected").text() + """</strong>?</p>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
						<button type="button" class="btn btn-danger confirm">Tak</button>
					  </div>
					</div>
				  </div>
				</div>
			"""
			$(html).modal()
			$(document).on "click", '#confirmationDialog .confirm', ->
				$.rails.confirmed link
				
	if window.location.href.indexOf("/subgroups/show_users") > 0 or
			window.location.href.indexOf("/subgroups/edit_users") > 0
			
		$(":checkbox").on "change", (event) ->
			select_name = "select[id='" + $(this).attr("id")
					.replace "_member", "_subgroup" + "']"
			
			if $(this).prop "checked"
				$(select_name).prop "disabled", false
				$(select_name + " option")
					.filter ->
						return this.value is ""
					.remove()
			else
				$(select_name).prop "disabled", true
				$("<option>", { value: '', selected: true })
					.prependTo(select_name)
				