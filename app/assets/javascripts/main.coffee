$(document).on "turbolinks:load", ->
	$("#main.panel-body").css "min-height", "250px"
	size_corecting = $("#main.panel").outerHeight(true) -
			$("#main.panel-body").outerHeight(true) +
			$("#main.panel").position().top
	$("#main.panel-body").css("max-height", "calc(100vh - " +
			size_corecting + "px)")