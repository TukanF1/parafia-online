$(document).on "turbolinks:load", ->
	if window.location.href.indexOf("/calendar/add_event") > 0
		$(":checkbox").on "change", (event) ->
			field_number_name = "input[id='" + $(this).attr("id")
				.replace "_mandatory_presence", "_number_mandatory" + "']"
			
			if $(this).prop "checked"
				$(field_number_name).prop "disabled", false
				$(field_number_name).val 1
			else
				$(field_number_name).prop "disabled", true
				$(field_number_name).val ""
				
	if window.location.href.indexOf("/calendar/event_categories") > 0
		
		window.categoryName = ""
		window.scheduleTypeChecked = ""
		window.original = true
		
		$("#selected_category").on "change click", (event) ->
			$("#category_weekly").hide()
			$("#category_group").hide()
			$("#category_none").hide()
			if window.categoryName isnt "" and window.scheduleTypeChecked isnt ""
				$("input:radio[name='" + window.categoryName + "']")
					.prop "checked", false
				$("input:radio[name='" + window.categoryName + "'][value='" + 
					window.scheduleTypeChecked + "']")
					.prop "checked", true
				window.categoryName = ""
				window.scheduleTypeChecked = ""
			if $("#selected_category_name").val() isnt ""
				$("[id='" + $("#selected_category_name").val() + "_weekly']").hide()
				$("[id='" + $("#selected_category_name").val() + "_group']").hide()
				$("[id='" + $("#selected_category_name").val() + "_none']").hide()
			$("#selected_category_name").val $("#selected_category :selected").text()
			$("#event_category_name").val $("#selected_category_name").val()
			if $("#selected_category_name").val() isnt ""
				$("[id='" + $("#selected_category_name").val() + "_weekly']").show()
				$("[id='" + $("#selected_category_name").val() + "_group']").show()
				$("[id='" + $("#selected_category_name").val() + "_none']").show()
			window.categoryName =
				$("input:radio[name='" + $("#selected_category_name").val() +
						"[schedule_type]']")
						.attr "name"
			if $("input:radio[name='" + window.categoryName + "'][value='weekly']")
					.prop("checked")
				window.scheduleTypeChecked = "weekly"
			else
				if $("input:radio[name='" + window.categoryName + "'][value='group']")
						.prop("checked")
					window.scheduleTypeChecked = "group"
				else
					if $("input:radio[name='" + window.categoryName + "'][value='none']")
							.prop("checked")
						window.scheduleTypeChecked = "none"
			$("input[name='edit']").attr disabled: true
			$("input[name='add']").attr disabled: true
			$("input[name='delete']").attr disabled: false
		
		$(":radio").on "change", (event) ->
			window.original = false
			if $("#event_category_name").val() isnt "" and
					$("#selected_category :selected").text() isnt ""
				$("input[name='edit']").attr disabled: false
			if window.categoryName is $(this).attr "name"
				if window.scheduleTypeChecked is $(this).val()
					window.original = true
					if $("#event_category_name").val() is
							$("#selected_category :selected").text()
						$("input[name='edit']").attr disabled: true
		
		$("#event_category_name").on "keyup", (event) ->
			if $("#event_category_name").val() isnt ""
				if $("#selected_category :selected").text() isnt ""
					if $("#event_category_name").val() is
							$("#selected_category :selected").text()
						if window.original
							$("input[name='edit']").attr disabled: true
						else
							$("input[name='edit']").attr disabled: false
						$("input[name='add']").attr disabled: true
					else
						if $("#selected_category option").filter(->
									$(this).text() == $("#event_category_name").val()
								).length
							$("input[name='add']").attr disabled: true
							$("input[name='edit']").attr disabled: true
						else
							$("input[name='add']").attr disabled: false
							$("input[name='edit']").attr disabled: false
				else
					$("input[name='edit']").attr disabled: true
					if $("#selected_category option").filter(->
								$(this).text() == $("#event_category_name").val()
							).length
						$("input[name='add']").attr disabled: true
					else
						$("input[name='add']").attr disabled: false
			else
				$("input[name='add']").attr disabled: true
				$("input[name='edit']").attr disabled: true
		
	if window.location.href.indexOf("/calendar/show_event_to_edit") > 0 or
			window.location.href.indexOf("/calendar/show_event") > 0 or
			window.location.href.indexOf("calendar/event_categories") > 0
		if window.location.href.indexOf("/calendar/show_event_to_edit") > 0
			$(":checkbox").on "change", (event) ->
				field_number_name = "input[id='" + $(this).attr("id")
					.replace "_mandatory_presence", "_number_mandatory" + "']"
				
				if $(this).prop "checked"
					$(field_number_name).prop "disabled", false
					$(field_number_name).val 1
				else
					$(field_number_name).prop "disabled", true
					$(field_number_name).val ""
			
			
		
		$.rails.allowAction = (link) ->
			return true unless link.attr "data-confirm"
			$.rails.showConfirmDialog link
			false
		
		$.rails.confirmed = (link) ->
			link.removeAttr "data-confirm"
			link.trigger "click.rails"
			$form.submit()
		
		$.rails.showConfirmDialog = (link) ->
			message = link.attr "data-confirm"
			html = """
				<div class="modal fade" tabindex="-1" role="dialog" id="confirmationDialog">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
						<h4 class="modal-title">Usunąć?</h4>
					  </div>
					  <div class="modal-body">
						<p>#{message}?</p>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
						<button type="button" class="btn btn-danger confirm">Tak</button>
					  </div>
					</div>
				  </div>
				</div>
			"""
			$(html).modal()
			$(document).on "click", '#confirmationDialog .confirm', ->
				$.rails.confirmed link
	
	if window.location.href
			.indexOf("/calendar/edit_mandatory_attendance_list") > 0 or
			window.location.href.indexOf("/calendar/edit_attendance_list") > 0
		$("div[id='add']").on "click", (event) ->
			select = $("#select_model").children().parent().html()
			$("#list").append "<div class='form-group'>" + select + "</div>"