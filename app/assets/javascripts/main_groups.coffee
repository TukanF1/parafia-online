# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "turbolinks:load", ->
	if window.location.href.indexOf("/main_groups/") > 0
		$("#selected_group").on "change click", (event) ->
			$("#main_group_name").val $("#selected_group :selected").text()
			$("input[name='edit']").attr disabled: true
			$("input[name='add']").attr disabled: true
			$("input[name='delete']").attr disabled: false
		
		$("#main_group_name").on "keyup", (event) ->
			if $("#main_group_name").val() isnt ""
				if $("#selected_group :selected").text() isnt ""
					if $("#main_group_name").val() is $("#selected_group :selected").text()
						$("input[name='edit']").attr disabled: true
						$("input[name='add']").attr disabled: true
					else
						if $("#selected_group option").filter(->
									$(this).text() == $("#main_group_name").val()
								).length
							$("input[name='add']").attr disabled: true
							$("input[name='edit']").attr disabled: true
						else
							$("input[name='add']").attr disabled: false
							$("input[name='edit']").attr disabled: false
				else
					$("input[name='edit']").attr disabled: true
					if $("#selected_group option").filter(->
								$(this).text() == $("#main_group_name").val()
							).length
						$("input[name='add']").attr disabled: true
					else
						$("input[name='add']").attr disabled: false
			else
				$("input[name='add']").attr disabled: true
				$("input[name='edit']").attr disabled: true
		
		$.rails.allowAction = (link) ->
			return true unless link.attr "data-confirm"
			$.rails.showConfirmDialog link
			false
		
		$.rails.confirmed = (link) ->
			link.removeAttr "data-confirm"
			link.trigger "click.rails"
			$form.submit()
		
		$.rails.showConfirmDialog = (link) ->
			message = link.attr "data-confirm"
			html = """
				<div class="modal fade" tabindex="-1" role="dialog" id="confirmationDialog">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
						<h4 class="modal-title">Usunąć?</h4>
					  </div>
					  <div class="modal-body">
						<p>#{message} <strong>""" + $("#selected_group :selected").text() + """</strong>?</p>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
						<button type="button" class="btn btn-danger confirm">Tak</button>
					  </div>
					</div>
				  </div>
				</div>
			"""
			$(html).modal()
			$(document).on "click", '#confirmationDialog .confirm', ->
				$.rails.confirmed link