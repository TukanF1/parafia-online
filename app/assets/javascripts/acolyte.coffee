$(document).on "turbolinks:load", ->
	if window.location.href.indexOf("/acolyte/edit_schedule") > 0
		$("div[id^='add_']").on "click", (event) ->
			div_id = $(this).attr("id").replace "add_", "cell_"
			element_div = "div[id='" + div_id + "']"
			select_id = div_id.replace("cell_", "") + "_"
			select_id_split = select_id.split("_")
			select_name = select_id_split[0] + "[" + select_id_split[1] + "][" +
				select_id_split[2] + "][]"
			select = $("#select_model").children()
				.attr("id", select_id)
				.attr("name", select_name)
				.parent().html()
			$(element_div).append "<div class='form-group'>" + select + "</div>"
	
	if window.location.href.indexOf("/acolyte/index") > 0 or
			window.location.href.indexOf("/acolyte/show_news") > 0
		$.rails.allowAction = (link) ->
			return true unless link.attr "data-confirm"
			$.rails.showConfirmDialog link
			false
		
		$.rails.confirmed = (link) ->
			link.removeAttr "data-confirm"
			link.trigger "click.rails"
			$form.submit()
		
		$.rails.showConfirmDialog = (link) ->
			message = link.attr "data-confirm"
			html = """
				<div class="modal fade" tabindex="-1" role="dialog" id="confirmationDialog">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
						<h4 class="modal-title">Usunąć?</h4>
					  </div>
					  <div class="modal-body">
						<p>#{message}?</p>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
						<button type="button" class="btn btn-danger confirm">Tak</button>
					  </div>
					</div>
				  </div>
				</div>
			"""
			$(html).modal()
			$(document).on "click", '#confirmationDialog .confirm', ->
				$.rails.confirmed link