$(document).on "turbolinks:load", ->
	if window.location.href.indexOf("/users/edit") > 0
		$.rails.allowAction = (link) ->
			return true unless link.attr "data-confirm"
			$.rails.showConfirmDialog link
			false
		
		$.rails.confirmed = (link) ->
			link.removeAttr "data-confirm"
			link.trigger "click.rails"
			$form.submit()
		
		$.rails.showConfirmDialog = (link) ->
			message = link.attr "data-confirm"
			html = """
				<div class="modal fade" tabindex="-1" role="dialog" id="confirmationDialog">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
						<h4 class="modal-title">Usunąć?</h4>
					  </div>
					  <div class="modal-body">
						<p>#{message}?</p>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
						<button type="button" class="btn btn-danger confirm">Tak</button>
					  </div>
					</div>
				  </div>
				</div>
			"""
			$(html).modal()
			$(document).on "click", '#confirmationDialog .confirm', ->
				$.rails.confirmed link