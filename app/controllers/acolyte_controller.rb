class AcolyteController < ApplicationController
	def index
		@access_admin = access_admin
		@access_moderator = access_moderator
		@current_group = MainGroup.find_by(name: "Ministranci")
		@page = params[:page].nil? ? 1 : params[:page].to_i
		@per_page = params[:per_page].nil? ? 5 : params[:per_page].to_i
		@pages_to_show = Array.new
		if @current_group.news.size > 0
			@news = @current_group.news.reverse.to_a
			@news_to_show = Array.new
			@pages = (@news.size / @per_page).to_i +
					(@news.size % @per_page > 0 ? 1 : 0)
			if @page > @pages
				@page = @pages
			elsif @page < 1
				@page = 1
			end
			((@page - 1) * @per_page).upto(@page * @per_page - 1) do |i|
				if @news.size > i
					@news_to_show << @news[i]
				end
			end
			@counter = 0
			((@page - 3)..(@page + 3)).each do |i|
				if i > 0 and i < @pages + 1
					@pages_to_show << i
					@counter += 1
				end
			end
			if @counter < @pages and @counter < 7
				(1..(7 - @counter)).each do |i|
					if @page - 3 < 1
						@pages_to_show << (@page + 3 + i)
					elsif @page + 3 > @pages
						@pages_to_show << (@page - 3 - i)
					end
				end
			end
			@pages_to_show.sort!
		else
			@page = 1
			@pages = 1
			@pages_to_show << 1
		end
	end
	
	def user_offer_self_work
		@categories = Array.new
		@current_group = MainGroup.find_by(name: "Ministranci")
		unless current_user_is_acolyte
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_index_url
		else
			@current_group.event_properties.where("mandatory_presence > ?", 0)
					.each do |property|
				if !@categories.include? property.event.event_category
					@categories << property.event.event_category
				end
			end
		end
	end
	
	def save_dispositions
		@current_group = MainGroup.find_by(name: "Ministranci")
		unless current_user_is_acolyte
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_index_url
		else
			current_user.dispositions.clear()
			EventCategory.all.each do |category|
				unless params[category.id.to_s].nil?
					params[category.id.to_s][:weekday].each do |weekday|
						params[category.id.to_s][:hour].each do |hour|
							if params[category.id.to_s][weekday][hour] == "1"
								@disposition = current_user.dispositions
									.build({weekday: weekday, hour: hour.to_s})
								@disposition.save!
								category.dispositions << @disposition
							end
						end
					end
				end
			end
			return redirect_to acolyte_user_offer_self_work_url
		end
	end
	
	def show_schedule
		@categories = Array.new
		@current_group = MainGroup.find_by(name: "Ministranci")
		unless current_user_is_acolyte
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_index_url
		else
			@current_group.event_properties.where("mandatory_presence > ?", 0)
					.each do |property|
				if !@categories.include? property.event.event_category
					@categories << property.event.event_category
				end
			end
		end
	end
	
	def generate_schedule
		@current_group = MainGroup.find_by(name: "Ministranci")
		@categories = Array.new
		unless access_admin
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_show_schedule_url
		else
			@current_group.work_plans.destroy_all
			@current_group.event_properties.where("mandatory_presence > ?", 0)
					.each do |property|
				if !@categories.include? property.event.event_category
					@categories << property.event.event_category
				end
			end
			@categories.each do |category|
				if category.schedule_type == "weekly"
					@days = Array.new
					@hours_in_days = Array.new
					@hours = Array.new
					category.events.each do |event|
						unless event.mandatory_attendance_list.nil?
							event.mandatory_attendance_list.destroy!
							event.mandatory_attendance_list = nil
							event.save!
							unless event.mandatory_attendance_list.nil?
								byebug
							end
						end
						@event_ok = false
						event.event_properties.where("mandatory_presence > ?", 0)
								.each do |property|
							if @event_ok = property.main_group.id == @current_group.id
								break
							end
						end
						if @event_ok
							if !@days.include? event.start_time.wday
								@days << event.start_time.wday
								@hours_in_days[event.start_time.wday] = Array.new
							end
							if !@hours.include? event.start_time.to_s(:time)
								@hours << event.start_time.to_s(:time)
							end
						end
					end
					@days.sort!
					@hours.sort!.each do |hour|
						@days.each do |weekday|
							category.events.each do |event|
								@event_ok = false
								event.event_properties.where("mandatory_presence > ?", 0)
										.each do |property|
									if @event_ok = property.main_group.id == @current_group.id
										break
									end
								end
								if @event_ok
									if event.start_time.wday == weekday and
											event.start_time.to_s(:time) == hour
										@current_group.users.each do |user|
											if !user.dispositions
													.find_by(weekday: weekday, hour: hour).nil?
												@work_plan = user.work_plans
													.find_by(main_group_id: @current_group.id)
												if @work_plan.nil?
													@work_plan = user.work_plans.build()
													@work_plan.save!
													@current_group.work_plans << @work_plan
												end
												@task = @work_plan.tasks.build()
												@task.save!
												event.tasks << @task
												@man_att_list = event.mandatory_attendance_list
												if @man_att_list.nil?
													@man_att_list = 
															event.build_mandatory_attendance_list
													@man_att_list.save!
												end
												@man_att_list.users << user
											end
										end
									end	
								end
							end
						end
					end
				end
			end
			return redirect_to acolyte_show_schedule_url
		end
	end
	
	def edit_schedule
		@categories = Array.new
		@current_group = MainGroup.find_by(name: "Ministranci")
		unless access_admin
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_index_url
		else
			@current_group.event_properties.where("mandatory_presence > ?", 0)
					.each do |property|
				if !@categories.include? property.event.event_category
					@categories << property.event.event_category
				end
			end
		end
	end
	
	def save_schedule
		@current_group = MainGroup.find_by(name: "Ministranci")
		unless access_admin
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_index_url
		else
			@current_group.work_plans.destroy_all
			@current_group.save!
			EventCategory.all.each do |category|
				if category.schedule_type == "weekly"
					category.events.each do |event|
						unless event.mandatory_attendance_list.nil?
							event.mandatory_attendance_list.destroy!
							event.mandatory_attendance_list = nil
							event.save!
							unless event.mandatory_attendance_list.nil?
								byebug
							end
						end
					end
					params[category.id.to_s][:weekday].each do |weekday|
						params[category.id.to_s][:hour].each do |hour|
							@events = Array.new
							category.events.each do |event|
								if event.start_time.wday.to_s == weekday and
										event.start_time.to_s(:time) == hour
									@events << event
								end
							end
							if !params[category.id.to_s].nil? and
									!params[category.id.to_s][weekday.to_s].nil? and
									!params[category.id.to_s][weekday.to_s][hour.to_s].nil?
								params[category.id.to_s][weekday.to_s][hour.to_s]
										.each do |user_id|
									if user_id != ""
										@user = User.find_by(id: user_id.to_i)
										if !@user.nil?
											@work_plan = @user.work_plans
												.find_by(main_group_id: @current_group.id)
											@work_plan_nil = false
											if @work_plan.nil?
												@work_plan = @user.work_plans.build()
												@work_plan.save!
												@current_group.work_plans << @work_plan
												@work_plan.save!
												@current_group.save!
												@work_plan_nil = true
											end
											# @task = @work_plan.tasks.build()
											# @task.save!
											@none_event = true
											@events.each do |event|
												if @work_plan.tasks.find_by(event_id: event.id).nil?
													@task = @work_plan.tasks.build()
													@task.save!
													event.tasks << @task
													@task.save!
													event.save!
													@none_event = false
													@man_att_list = event.mandatory_attendance_list
													if @man_att_list.nil?
														@man_att_list =
																event.build_mandatory_attendance_list
														@man_att_list.save!
														event.save!
													end
													if @man_att_list.users.find_by(id: @user.id).nil?
														@man_att_list.users << @user
														@man_att_list.save!
														@user.save!
														event.save!
													end
												end
											end
											if @none_event
												# @work_plan.tasks.destroy(@task)
												if @work_plan_nil
													@user.work_plans.destroy(@work_plan)
												end
											end
										end
									end
								end
							end
						end
					end
				end
			end
			return redirect_to acolyte_show_schedule_url
		end
	end
	
	def select_date_ranking
	end
	
	def show_ranking
		@current_group = MainGroup.find_by(name: "Ministranci")
		@start = Date.new params[:start]["date(1i)"].to_i,
				params[:start]["date(2i)"].to_i,
				params[:start]["date(3i)"].to_i
		@end = Date.new params[:end]["date(1i)"].to_i,
				params[:end]["date(2i)"].to_i,
				params[:end]["date(3i)"].to_i
		@events = Array.new
		Event.all.each do |event|
			if event.start_time >= @start.beginning_of_day and
					event.start_time <= @end.end_of_day
				@events << event
			end
		end
	end
	
	def write_news
		unless access_moderator or access_admin
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_index_url
		end
		@current_group = MainGroup.find_by(name: "Ministranci")
		@subgroup = current_user.subgroups.find_by(main_group_id: @current_group.id)
	end
	
	def create_news
		@current_group = MainGroup.find_by(name: "Ministranci")
		@subgroup = current_user.subgroups.find_by(main_group_id: @current_group.id)
		if access_moderator or access_admin
			@news = @current_group.news.build()
			@news.introduction = params[:news][:content].gsub("\r\n", " ")
					.truncate_words(60)
			params[:news][:content] = params[:news][:content].gsub "\r\n", "<br />"
			@news.title = params[:news][:title]
			@news.content = params[:news][:content]
			@news.author = current_user
			if params[:news][:content] == "" or params[:news][:title] == "" or
					@news.title.nil? or @news.content.nil?
				flash[:alert] = "Pola tektowe nie mogą być puste."
				return redirect_to acolyte_index_url
			else
				@news.save!
				flash[:notice] = "Wiadomość została poprawnie dodana."
				return redirect_to acolyte_index_url
			end
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_index_url
		end
	end
	
	def delete_news
		@current_group = MainGroup.find_by(name: "Ministranci")
		@subgroup = current_user.subgroups.find_by(main_group_id: @current_group.id)
		if access_admin
			@news = News.find_by(id: params[:id])
			if @news.nil?
				flash[:alert] = "Nie znaleziono wiadomości."
				return redirect_to acolyte_index_url
			end
			@news.destroy
			flash[:notice] = "Wiadomość została usunięta."
			return redirect_to acolyte_index_url
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to acolyte_index_url
		end
	end
	
	def show_news
		@access_admin = access_admin
		@news = nil
		unless params[:id].nil?
			@news = News.find(params[:id])
		end
		if @news.nil?
			flash[:alert] = "Wiadomość nie została znaleziona."
			return redirect_to acolyte_index_url(page: params[:page])
		end
	end
	
	private
	def current_user_is_acolyte
		return (user_signed_in? and
				!current_user.main_groups.find_by(name: "Ministranci").nil?)
	end
	
	private
	def current_user_is_developer
		return !current_user.subgroups.find_by(main_group_id:
				MainGroup.find_by(name: "Deweloperzy")).nil?
	end
	
	private
	def current_user_is_main_admin
		return !current_user.subgroups.find_by(main_group_id:
				MainGroup.find_by(name: "Użytkownicy"), admin: true).nil?
	end
	
	private
	def current_user_is_main_moderator
		return !current_user.subgroups.find_by(main_group_id:
				MainGroup.find_by(name: "Użytkownicy"), moderator: true).nil?
	end
	
	private
	def current_user_is_group_admin
		return !current_user.subgroups.find_by(main_group_id:
				MainGroup.find_by(name: "Ministranci"), admin: true).nil?
	end
	
	private
	def current_user_is_group_moderator
		return !current_user.subgroups.find_by(main_group_id:
				MainGroup.find_by(name: "Ministranci"), moderator: true).nil?
	end
	
	private
	def access_admin
		return (user_signed_in? and (current_user_is_developer or
				current_user_is_group_admin or current_user_is_main_admin))
	end
	
	private
	def access_moderator
		return (user_signed_in? and (current_user_is_developer or
				current_user_is_group_moderator or current_user_is_main_moderator))
	end
	
end
