class HomeController < ApplicationController
  PARAM_KEY_BLACKLIST = :authenticity_token, :commit, :utf8, :_method, :script_name
  
  def index
    @params = @view_context.respond_to?(:params) ? @view_context.params : Hash.new
    @params = @params.to_unsafe_h if @params.respond_to?(:to_unsafe_h)
    @params = @params.with_indifferent_access.except(*PARAM_KEY_BLACKLIST)
  end
end
