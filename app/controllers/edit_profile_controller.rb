class EditProfileController < Devise::RegistrationsController

protected
	def update_resource(resource, params)
		if params[:password].blank? &&
				(params[:email].blank? || params[:email] == resource.email)
			params.delete :password
			params.delete :password_confirmation
			params.delete :current_password
			params.delete :email
			resource.update_without_password(params)
		else
			params.delete :first_name
			params.delete :second_name
			params.delete :last_name
			params.delete :date_of_birth
			params.delete :is_female
			super
		end
	end
end