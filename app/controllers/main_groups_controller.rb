class MainGroupsController < ApplicationController
	before_action :authenticate_user!
	
	def manage
		if current_user.subgroups.find_by(
				main_group_id: MainGroup.find_by(name: "Deweloperzy")).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
	end
	
	def edit
		if current_user.subgroups.find_by(
				main_group_id: MainGroup.find_by(name: "Deweloperzy")).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		if params[:delete]
			destroy
		elsif params[:main_group][:name] != ""
			if params[:edit]
				if params[:selected_group].nil?
					create
				else
					@founded_group = MainGroup.find(params[:selected_group])
					if @founded_group.name != params[:main_group][:name]
						if @founded_group.update_attributes(main_group_param)
							flash[:notice] = "Zmieniono nazwę grupy."
							return redirect_to main_groups_manage_url
						else
							respond_to do |format|
								flash[:alert] = "Edycja się nie powiodła."
								return format.html { render :manage }
							end
						end
					end
				end
			elsif params[:add]
				create
			end
			flash[:alert] = "Wystąpił błąd wyboru akcji."
			return redirect_to main_groups_manage_url
		else
			respond_to do |format|
				flash[:alert] = "Nazwa grupy nie może być pusta."
				return format.html { render :manage }
			end
		end
	end
	
	def create
		if current_user.subgroups.find_by(
				main_group_id: MainGroup.find_by(name: "Deweloperzy")).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		if params[:main_group][:name] != ""
			@group = MainGroup.new(main_group_param)
			begin
				@group.save!
				flash[:notice] = "Stworzono nową grupę."
				redirect_to main_groups_manage_url
			rescue ActiveRecord::RecordNotUnique
				respond_to do |format|
					flash[:alert] = "Grupa o tej nazwie już istnieje."
					return format.html { render :manage }
				end
			end
		else
			respond_to do |format|
				flash[:alert] = "Nazwa grupy nie może być pusta."
				return format.html { render :manage }
			end
		end
	end
	
	def destroy
		if current_user.subgroups.find_by(
				main_group_id: MainGroup.find_by(name: "Deweloperzy")).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		@founded_group = MainGroup.find_by(id: params[:selected_group].to_i)
		unless @founded_group.nil?
			@founded_group.destroy
			flash[:notice] = "Usunięto grupę."
		else
			flash[:alert] = "Wybrana grupa nie istnieje."
		end
		return redirect_to main_groups_manage_url
	end
	
	private
	def main_group_param
		params.require(:main_group).permit(:name)
	end
end
