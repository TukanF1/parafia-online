class SubgroupsController < ApplicationController
	before_action :authenticate_user!
	
	def manage
		if current_user.subgroups.find_by(
				main_group_id: MainGroup.find_by(name: "Deweloperzy")).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
	end
	
	def edit
		if current_user.subgroups.find_by(
				main_group_id: MainGroup.find_by(name: "Deweloperzy")).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		if params[:delete]
			destroy
		elsif params[:subgroup][:name] != ""
			if params[:edit]
				if params[:selected_subgroup].nil?
					create
				else
					@founded_subgroup = Subgroup.find_by(
						name: params[:selected_subgroup_name])
					if (@founded_subgroup.name != params[:subgroup][:name] or
							@founded_subgroup.admin !=
								params[params[:selected_subgroup_name]][:admin] or
							@founded_subgroup.moderator !=
								params[params[:selected_subgroup_name]][:moderator]) and
							@founded_subgroup.update_attributes(subgroup_param)
						flash[:notice] = "Parametry rangi zostały zmienione."
						return redirect_to subgroups_manage_url
					else
						respond_to do |format|
							flash[:alert] = "Edycja się nie powiodła."
							return format.html { render :manage }
						end
					end
				end
			elsif params[:add]
				create
			end
		else
			respond_to do |format|
				flash[:alert] = "Nazwa rangi nie może być pusta."
				return format.html { render :manage }
			end
		end
	end
	
	def create
		if current_user.subgroups.find_by(
				main_group_id: MainGroup.find_by(name: "Deweloperzy")).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		if params[:subgroup][:name] != ""
			@founded_main_group = MainGroup.find(params[:selected_main_group])
			@group = @founded_main_group.subgroups.build(subgroup_param)
			begin
				@group.save!
				flash[:notice] = "Stworzono nową grupę."
				return redirect_to subgroups_manage_url
			rescue ActiveRecord::RecordNotUnique
				respond_to do |format|
					flash[:alert] = "Ranga o tej nazwie już istnieje."
					return format.html { render :manage }
				end
			end
		else
			respond_to do |format|
				flash[:alert] = "Nazwa rangi nie może być pusta."
				return format.html { render :manage }
			end
		end
	end
	
	def destroy
		if current_user.subgroups.find_by(
				main_group_id: MainGroup.find_by(name: "Deweloperzy")).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		@founded_subgroup = Subgroup.find_by(name: params[:selected_subgroup_name])
		@founded_subgroup.destroy
		flash[:notice] = "Usunięto rangę."
		return redirect_to subgroups_manage_url
	end
	
	def show_users
		@current_group = MainGroup.find_by(name: params[:group_name])
		@user_subgroup = current_user.subgroups.find_by(
			main_group_id: @current_group.id)
		if @current_group.nil?
			flash[:alert] = "Błąd dostępu poprzez nazwę grupy."
			return redirect_to root_url
		elsif (@user_subgroup.nil? or @user_subgroup.admin == false) and
				current_user.subgroups.find_by(main_group_id:
					MainGroup.find_by(name: "Deweloperzy").id).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
	end
	
	def edit_users
		@current_group = MainGroup.find_by(name: params[:group_name])
		@user_subgroup = current_user.subgroups.find_by(
			main_group_id: @current_group.id)
		if @current_group.nil?
			flash[:alert] = "Błąd dostępu poprzez nazwę grupy."
			return redirect_to root_url
		elsif (@user_subgroup.nil? or @user_subgroup.admin == false) and
				current_user.subgroups.find_by(main_group_id:
					MainGroup.find_by(name: "Deweloperzy").id).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		User.all.each do |user|
			@subgroups = user.subgroups.find_by(main_group_id: @current_group.id)
			if params[user.email][:member] == "1"
				if @subgroups.nil?
					user.subgroups << Subgroup.find(params[user.email][:subgroup])
				else
					user.subgroups.delete(@subgroups)
					user.subgroups << Subgroup.find(params[user.email][:subgroup])
				end
			else if !@subgroups.nil?
					user.subgroups.delete(@subgroups)
				end
			end
		end
		flash[:notice] = "Lista członków grupy i ich rang została zapisana."
		return redirect_to subgroups_show_users_url(group_name: @current_group.name)
	end
	
	private
	def subgroup_param
		@sel_sub_name = params[:selected_subgroup_name]
		if @sel_sub_name != ""
			params[:subgroup][:admin] = params[@sel_sub_name][:admin]
			params[:subgroup][:moderator] = params[@sel_sub_name][:moderator]
		end
		params.require(:subgroup).permit(:name, :admin, :moderator)
	end
end
