class CalendarController < ApplicationController
	
	def show
		@events = Event.all
	end
	
	def show_event
		@event = Event.find(params[:id])
		if @event.nil?
			flash[:alert] = "Błąd. Spróbuj ponownie."
			return redirect_to request.referrer.nil? ?
					calendar_show_url : request.referrer
		end
		@access = (user_signed_in? and (current_user_is_main_admin or
				current_user_is_organizer(@event) or current_user_is_developer))
		@can_edit_attendance_list = (user_signed_in? and (@access or
			current_user_is_in_organizer_group(@event)))
	end
	
	def add_event
		if user_signed_in? and !current_user.subgroups.find_by(admin: true).nil?
			params[:start_time] = (Time.zone.parse params[:start_time]) + 8.hour
			if params[:end_time].nil?
				params[:end_time] = params[:start_time] + 1.hour
			end
			@event = Event.new(start_time: params[:start_time],
					end_time: params[:end_time])
			@main_groups_to_selected = Array.new
			unless current_user_is_main_admin or current_user_is_developer
				current_user.subgroups.where(admin: true).each do |subgroup|
					if subgroup.main_group.name != "Deweloperzy"
						@main_groups_to_selected << subgroup.main_group
					end
				end
			else
				@main_groups_to_selected = MainGroup.where.not(name: "Deweloperzy")
			end
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_url
		end
	end
	
	def create_event
		if user_signed_in? and !current_user.subgroups.find_by(admin: true).nil?
			if params[:selected_group].nil?
				flash[:alert] = "Nie wybrałeś organizatorów."
				return redirect_to request.referrer
			end
			params[:event][:start_time] = DateTime.civil_from_format :local,
				params[:event]["start_time(1i)"].to_i,
				params[:event]["start_time(2i)"].to_i,
				params[:event]["start_time(3i)"].to_i,
				params[:event]["start_time(4i)"].to_i,
				params[:event]["start_time(5i)"].to_i
			if params[:event]["end_time(4i)"].nil?
				params[:event][:end_time] = (params[:event][:start_time] + 1.day)
					.beginning_of_day()
			else
				params[:event][:end_time] = DateTime.civil_from_format :local,
					params[:event]["end_time(1i)"].to_i,
					params[:event]["end_time(2i)"].to_i,
					params[:event]["end_time(3i)"].to_i,
					params[:event]["end_time(4i)"].to_i,
					params[:event]["end_time(5i)"].to_i
			end
			params[:repeat_end] = DateTime.civil_from_format :local,
				params[:repeat_end]["(1i)"].to_i,
				params[:repeat_end]["(2i)"].to_i,
				params[:repeat_end]["(3i)"].to_i
			# params[:event][:start_time] =
			# 	Time.zone.parse params[:event][:start_time].to_s
			# params[:event][:end_time] =
			# 	Time.zone.parse params[:event][:end_time].to_s
			@founded_category = EventCategory.find(params[:selected_category])
			if params[:overriding_event] != ""
				@overriding_event = Event.find_by(id: params[:overriding_event].to_i)
			end
			if params[:repeat_every_number].to_i == 0
				@repeat = false
			else
				@repeat = true
			end
			@event = nil
			@event_previous = nil
			@date = nil
			begin
				unless @event.nil?
					@event_previous = @event
				end
				@params = params[:event]
				@params[:description] = @params[:description].gsub("\r\n", "<br />")
				@event = @founded_category.events.build({name: @params[:name],
					start_time: @params[:start_time].to_s(:db),
					end_time: @params[:end_time].to_s(:db),
					# start_time: @params[:start_time] - Time.zone.utc_offset.seconds,
					# end_time: @params[:end_time] - Time.zone.utc_offset.seconds,
					description: @params[:description], color: @params[:color]})
				# @event = @founded_category.events.build(event_params)
				@event.save!
				if @date.nil?
					@date = @event.start_time.to_date
				end
				unless @event_previous.nil?
					@event_previous.next_in_series_event = @event
				end
				MainGroup.all.each do |group|
					if params[group.id.to_s]
						@property = nil
						if params[group.id.to_s][:mandatory_presence].to_i > 0
							if !params[:selected_group].include?(group.id.to_s)
								@property = group.event_properties.build({organizer: false,
									mandatory_presence: params[group.id.to_s][:number_mandatory]})
							else
								@property = group.event_properties.build({organizer: true,
									mandatory_presence: params[group.id.to_s][:number_mandatory]})
							end
							@property.save!
							@event.event_properties << @property
							@event.save!
						elsif params[:selected_group].include?(group.id.to_s)
							@property = group.event_properties.build({organizer: true,
								mandatory_presence: 0})
							@property.save!
							@event.event_properties << @property
						end
					end
				end
				if !@overriding_event.nil?
					@overriding_event.related_events << @event
					@overriding_event.save!
					@overriding_event = @overriding_event.next_in_series_event
				end
				@event.save!
				case params[:repeat_every_type].to_i
				when 0
					params[:event][:start_time] +=
						params[:repeat_every_number].to_i.days
					params[:event][:end_time] +=
						params[:repeat_every_number].to_i.days
				when 1
					params[:event][:start_time] +=
						params[:repeat_every_number].to_i.weeks
					params[:event][:end_time] +=
						params[:repeat_every_number].to_i.weeks
				when 2
					params[:event][:start_time] +=
						params[:repeat_every_number].to_i.months
					params[:event][:end_time] +=
						params[:repeat_every_number].to_i.months
				when 3
					params[:event][:start_time] +=
						params[:repeat_every_number].to_i.years
					params[:event][:end_time] +=
						params[:repeat_every_number].to_i.years
				end
				if params[:event][:start_time] > params[:repeat_end] + 1.day
					@repeat = false
				end
			end while @repeat
			flash[:notice] = "Utworzono nowe wydarzenie."
      return redirect_to calendar_show_url(start_date: @date)
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_url
		end
	end
	
	def show_event_to_edit
		@event = Event.find(params[:id])
		@date = @event.start_time.to_date
		unless user_signed_in? and (current_user_is_main_admin or
				current_user_is_organizer(@event) or current_user_is_developer)
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return calendar_show_url(start_date: @date)
		end
		@organizers = Array.new
		@main_groups_to_selected = Array.new
		@event.event_properties.where(organizer: true).each do
				|organizer_property|
			@organizers << organizer_property.main_group.id
		end
		unless current_user_is_main_admin or current_user_is_developer
			current_user.subgroups.where(admin: true).each do |subgroup|
				if subgroup.main_group.name != "Deweloperzy"
					@main_groups_to_selected << subgroup.main_group
				end
			end
		else
			@main_groups_to_selected = MainGroup.where.not(name: "Deweloperzy")
		end
		@event.description = @event.description.gsub("<br />", "\r\n")
	end
	
	def edit_event
		@event = Event.find(params[:event][:id])
		@date = @event.start_time.to_date
		if current_user_is_organizer(@event) or current_user_is_developer
			if params[:selected_group].nil?
				flash[:alert] = "Nie wybrałeś organizatorów."
				return redirect_to request.referrer
			end
			params[:event][:start_time] = DateTime.civil_from_format :local,
				params[:event]["start_time(1i)"].to_i,
				params[:event]["start_time(2i)"].to_i,
				params[:event]["start_time(3i)"].to_i,
				params[:event]["start_time(4i)"].to_i,
				params[:event]["start_time(5i)"].to_i
			if params[:event]["end_time(4i)"].nil?
				params[:event][:end_time] = params[:event][:start_time].end_of_day
			else
				params[:event][:end_time] = DateTime.civil_from_format :local,
					params[:event]["end_time(1i)"].to_i,
					params[:event]["end_time(2i)"].to_i,
					params[:event]["end_time(3i)"].to_i,
					params[:event]["end_time(4i)"].to_i,
					params[:event]["end_time(5i)"].to_i
			end
			params[:event][:start_time] =
				Time.zone.parse params[:event][:start_time].to_s
			params[:event][:end_time] = Time.zone.parse params[:event][:end_time].to_s
			@founded_category = EventCategory.find(params[:selected_category])
			params[:event][:description] =
					params[:event][:description].gsub("\r\n", "<br />")
			@event.update_attributes(event_params)
			if @event.event_category != @founded_category
				unless @event.event_category.nil?
					@event.event_category.events.delete(@event)
				end
				@founded_category.events << @event
			end
			MainGroup.all.each do |group|
				if params[group.id.to_s]
					@property = @event.event_properties.find_by(main_group_id: group.id)
					if params[group.id.to_s][:mandatory_presence].to_i > 0
						if !params[:selected_group].include?(group.id.to_s)
							if !@property.nil?
								@property.update_attributes({organizer: false,
									mandatory_presence: params[group.id.to_s][:number_mandatory]})
							else
								@property = group.event_properties.build({organizer: false,
									mandatory_presence: params[group.id.to_s][:number_mandatory]})
								@property.save!
								@event.event_properties << @property
							end
						else
							if !@property.nil?
								@property.update_attributes({organizer: true,
									mandatory_presence: params[group.id.to_s][:number_mandatory]})
							else
								@property = group.event_properties.build({organizer: true,
									mandatory_presence: params[group.id.to_s][:number_mandatory]})
								@property.save!
								@event.event_properties << @property
							end
						end
						@event.save!
					elsif params[:selected_group].include?(group.id.to_s)
						if !@property.nil?
							@property.update_attributes(
								{organizer: true, mandatory_presence: 0})
						else
							@property = group.event_properties.build({organizer: true,
								mandatory_presence: 0})
							@property.save!
							@event.event_properties << @property
						end
						@event.save!
					elsif !params[:selected_group].include?(group.id.to_s) and
							!@event.main_groups.find_by(id: group.id).nil?
						@event.event_properties.delete(
							@event.event_properties.find_by(main_group_id: group.id))
					end
				elsif !@event.main_groups.find_by(id: group.id).nil?
					@event.event_properties.delete(
						@event.event_properties.find_by(main_group_id: group.id))
				end
			end
			@id_overriding_event = params[:overriding_event]
			if @id_overriding_event != ""
				@overriding_event = Event.find_by(id: @id_overriding_event.to_i)
				unless @overriding_event.nil?
					@event.overriding_event.related_events.delete(@event)
					@overriding_event.related_events << @event
					@overriding_event.save!
				end
			end
			@event.save!
			flash[:notice] = "Zapisano zmiany."
    	return redirect_to calendar_show_url(start_date: @date)
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_url(start_date: @date)
		end
	end
	
	def destroy_event
		@event = Event.find(params[:id])
		@date = @event.start_time.to_date
		@access = (current_user_is_organizer(@event) or current_user_is_developer)
		if !@access
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_url
		end
		unless @event.next_in_series_event.nil? or
				@event.previous_in_series_event.nil?
			@event.previous_in_series_event
					.next_in_series_event = @event.next_in_series_event
			@event.next_in_series_event
					.previous_in_series_event = @event.previous_in_series_event
		end
		@event.destroy
		flash[:notice] = "Usunięto wydarzenie."
		return redirect_to calendar_show_url(start_date: @date)
	end
	
	def destroy_event_series
		@event = Event.find(params[:id])
		@date = @event.start_time.to_date
		@access = (current_user_is_organizer(@event) or current_user_is_developer)
		if !@access
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_url
		end
		begin
			@event_next = @event.next_in_series_event
			@event.destroy
			@event = @event_next
		end while !@event.nil?
		flash[:notice] = "Usunięto wydarzenia."
		return redirect_to calendar_show_url(start_date: @date)
	end
	
	def event_categories
		if !user_signed_in?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
	end
	
	def event_category_edit
		if current_user.subgroups.find_by(admin: true).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		if params[:delete]
			event_category_destroy
		elsif params[:event_category][:name] != ""
			if params[:edit]
				if params[:selected_category].nil?
					event_category_create
				else
					@founded_category = EventCategory.find(params[:selected_category])
					if @founded_category.update_attributes(event_category_params)
						flash[:notice] = "Dokonano edycji kategorii."
						return redirect_to calendar_event_categories_url
					else
						respond_to do |format|
							flash[:alert] = "Edycja się nie powiodła."
							return format.html { render :event_categories }
						end
					end
				end
			elsif params[:add]
				event_category_create
			end
		else
			respond_to do |format|
				flash[:alert] = "Nazwa grupy nie może być pusta."
				return format.html { render :manage }
			end
		end
	end
	
	def event_category_create
		if current_user.subgroups.find_by(admin: true).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		if params[:event_category][:name] != ""
			@category = EventCategory.new(event_category_params)
			begin
				@category.save!
				flash[:notice] = "Stworzono nową kategorię."
				return redirect_to calendar_event_categories_url
			rescue ActiveRecord::RecordNotUnique
				respond_to do |format|
					flash[:alert] = "Kategoria o tej nazwie już istnieje."
					return format.html { render :event_categories }
				end
			end
		else
			respond_to do |format|
				flash[:alert] = "Nazwa kategorii nie może być pusta."
				return format.html { render :event_categories }
			end
		end
	end
	
	def event_category_destroy
		if current_user.main_groups.find_by(name: "Użytkownicy")
				.subgroups.find_by(admin: true).nil?
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to root_url
		end
		@founded_category = EventCategory.find(params[:selected_category])
		@founded_category.destroy
		flash[:notice] = "Usunięto kategorię."
		return redirect_to calendar_event_categories_url
	end
	
	def show_attendance_list
		@event = Event.find(params[:id])
		@current_group = MainGroup.find_by(name: "Ministranci")
		@man_att_list = @event.mandatory_attendance_list
		@user_subgroup = current_user.subgroups
				.find_by(main_group_id: @current_group.id)
		@user_is_acolyte = @user_subgroup.nil? ? false : true
		if @user_is_acolyte
			@user_can_edit = @user_subgroup.moderator or @user_subgroup.admin
		end
		if @man_att_list.nil?
			@man_att_list = @event.build_mandatory_attendance_list
			@man_att_list.save!
		end
		@att_list = @event.attendance_list
		if @att_list.nil?
			@att_list = @event.build_attendance_list
			@att_list.save!
		end
		@man_att = @man_att_list.users
		@att = @att_list.users
		if @man_att_list.users.size > 1
			@man_att =
					@man_att_list.users.sort_by { |u| u.last_name }
		end
		if @att_list.users.size > 1
			@att = @att_list.users.sort_by { |u| u.last_name }
		end
	end
	
	def edit_attendance_list
		@current_group = MainGroup.find_by(name: "Ministranci")
		@user_subgroup = current_user.subgroups
				.find_by(main_group_id: @current_group.id)
		@user_is_acolyte = @user_subgroup.nil? ? false : true
		if @user_is_acolyte
			@event = Event.find_by(id: params[:id])
			if @event.start_time < DateTime.current
				@att_list = @event.attendance_list
				if @att_list.nil?
					@att_list = @event.build_attendance_list
					@att_list.save!
				end
			else
				flash[:alert] = "Wydarzenie się jeszcze nie rozpoczęło!"
				return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
			end
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		end
	end
	
	def save_attendance_list
		@current_group = MainGroup.find_by(name: "Ministranci")
		@user_subgroup = current_user.subgroups
				.find_by(main_group_id: @current_group.id)
		@user_is_acolyte = @user_subgroup.nil? ? false : true
		if @user_is_acolyte
			@event = Event.find(params[:event_id])
			if @event.start_time < DateTime.current
				@att_list = @event.attendance_list
				unless @att_list.nil?
					@event.attendance_list.destroy
				end
				@att_list = @event.build_attendance_list
				@att_list.save!
				params[@event.id.to_s][:attendance].each do |user_id|
					unless user_id == ""
						@user = User.find_by(id: user_id.to_i)
						unless @user.nil?
							if @att_list.users.find_by(id: @user.id).nil?
								@att_list.users << @user
							end
						end
					end
				end
				@users_list = @att_list.users +
					(@event.mandatory_attendance_list.users - @att_list.users)
				@users_list.each do |user|
					@point = user.points.find_by(event_id: @event.id)
					if @point.nil?
						@point = user.points.build()
						@point.save!
						if @att_list.users.include? user and @event.mandatory_attendance_list.users.include? user
							@point.points = 1
						elsif @event.mandatory_attendance_list.users.include? user
							@point.points = -5
						elsif @att_list.users.include? user
							@point.points = 2
						else
							@point.points = 0
						end
						@event.points << @point
					end
				end
			else
				flash[:alert] = "Wydarzenie się jeszcze nie rozpoczęło!"
				return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
			end
			flash[:notice] = "Poprawnie zmieniono listę obecnosci."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		end
	end
	
	def edit_mandatory_attendance_list
		@event = Event.find_by(id: params[:id])
		@current_group = MainGroup.find_by(name: "Ministranci")
		@user_subgroup = current_user.subgroups
				.find_by(main_group_id: @current_group.id)
		@user_is_acolyte = @user_subgroup.nil? ? false : true
		if @user_is_acolyte
			@user_can_edit = @user_subgroup.moderator or @user_subgroup.admin
		end
		if @user_can_edit
			@man_att_list = @event.mandatory_attendance_list
			if @man_att_list.nil?
				@man_att_list = @event.build_mandatory_attendance_list
				@man_att_list.save!
			end
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		end
	end
	
	def save_mandatory_attendance_list
		@event = Event.find(params[:event_id])
		@current_group = MainGroup.find_by(name: "Ministranci")
		@user_subgroup = current_user.subgroups
				.find_by(main_group_id: @current_group.id)
		@user_is_acolyte = @user_subgroup.nil? ? false : true
		if @user_is_acolyte
			@user_can_edit = @user_subgroup.moderator or @user_subgroup.admin
		end
		if @user_can_edit
			@man_att_list = @event.mandatory_attendance_list
			unless @man_att_list.nil?
				@event.mandatory_attendance_list.destroy
			end
			@man_att_list = @event.build_mandatory_attendance_list
			@man_att_list.save!
			params[@event.id.to_s][:mandatory].each do |user_id|
				unless user_id == ""
					@user = User.find_by(id: user_id.to_i)
					unless @user.nil?
						if @man_att_list.users.find_by(id: @user.id).nil?
							@man_att_list.users << @user
						end
					end
				end
			end
			unless @event.attendance_list.nil? and @event.start_time > DateTime.current
				@users_list = @event.attendance_list.users +
					(@man_att_list.users - @event.attendance_list.users)
				@users_list.each do |user|
					@point = user.points.find_by(event_id: @event.id)
					if @point.nil?
						@point = user.points.build()
						@point.save!
						if @event.attendance_list.users.include? user and @man_att_list.users.include? user
							@point.points = 1
						elsif @man_att_list.users.include? user
							@point.points = -5
						elsif @event.attendance_list.users.include? user
							@point.points = 2
						else
							@point.points = 0
						end
						@event.points << @point
					end
				end
			end
			flash[:notice] = "Poprawnie zmieniono listę obecnosci obowiązkowej."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		end
	end
	
	def edit_points
		@current_group = MainGroup.find_by(name: "Ministranci")
		@user_subgroup = current_user.subgroups
				.find_by(main_group_id: @current_group.id)
		@user_is_acolyte = @user_subgroup.nil? ? false : true
		if @user_is_acolyte
			@user_can_edit = @user_subgroup.moderator or @user_subgroup.admin
		end
		if @user_can_edit
			@event = Event.find(params[:id])
			@users_list = @event.attendance_list.users +
				(@event.mandatory_attendance_list.users - @event.attendance_list.users)
			if @users_list.nil?
				@users_list = @event.build_attendance_list
				@event.build_mandatory_attendance_list
				@users_list.save!
			end
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		end
	end
	
	def save_points
		@current_group = MainGroup.find_by(name: "Ministranci")
		@user_subgroup = current_user.subgroups
				.find_by(main_group_id: @current_group.id)
		@user_is_acolyte = @user_subgroup.nil? ? false : true
		if @user_is_acolyte
			@event = Event.find(params[:event_id])
			@current_group = MainGroup.find_by(name: "Ministranci")
			@users_list = @event.attendance_list.users +
				(@event.mandatory_attendance_list.users - @event.attendance_list.users)
			unless @users_list.nil?
				@users_list.each do |user|
					@point = user.points.find_by(event_id: @event.id)
					if @point.nil?
						@point = user.points.build()
						@point.save!
					end
					@point.points = params[@event.id.to_s][:points][user.id.to_s].to_i
					@event.points << @point
				end
				flash[:notice] = "Punkty zostały zapisane."
				return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
			end
			flash[:alert] = "Błąd zapisu punktów z powodu pustej listy użytkowników."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		else
			flash[:alert] = "Nie masz uprawnień do przeglądania tej podstrony."
			return redirect_to calendar_show_attendance_list_url(id: params[:event_id])
		end
	end
	
	private
	def current_user_is_organizer(event)
		if !user_signed_in?
			return false
		end
		if event.class == Event
			@organizers = event.event_properties.where(organizer: true)
			@user_subgroups = current_user.subgroups.where(admin: true)
			[*@user_subgroups].each do |subgroup|
				[*@organizers].each do |organizer|
					if subgroup.main_group == organizer.main_group
						return true
					end
				end
			end
		end
		return false
	end
	
	private
	def current_user_is_in_organizer_group(event)
		if !user_signed_in?
			return false
		end
		if event.class == Event
			@organizers = event.event_properties.where(organizer: true)
			@user_subgroups = current_user.subgroups
			[*@user_subgroups].each do |subgroup|
				[*@organizers].each do |organizer|
					if subgroup.main_group == organizer.main_group
						return true
					end
				end
			end
		end
		return false
	end
		
	private
	def current_user_is_developer
		return !current_user.subgroups.find_by(main_group_id:
				MainGroup.find_by(name: "Deweloperzy")).nil?
	end
	
	private
	def current_user_is_main_admin
		return !current_user.subgroups.find_by(main_group_id:
				MainGroup.find_by(name: "Użytkownicy"), admin: true).nil?
	end
	
	private
	def event_params
		params.require(:event).permit(:name, :start_time, :end_time, :description,
			:color)
	end
	
	private
	def event_category_params
		@sel_cat_name = params[:selected_category_name]
		if @sel_cat_name != ""
			params[:event_category][:schedule_type] =
					params[@sel_cat_name][:schedule_type]
		end
		params.require(:event_category).permit(:name, :schedule_type)
	end
	
	private
	def event_property_params
		params.require(:property).permit(:organizer, :number_mandatory)
	end
end