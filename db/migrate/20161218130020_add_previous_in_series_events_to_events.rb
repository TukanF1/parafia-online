class AddPreviousInSeriesEventsToEvents < ActiveRecord::Migration
  def change
    add_reference :events, :previous_in_series_event, index: true
  end
end
