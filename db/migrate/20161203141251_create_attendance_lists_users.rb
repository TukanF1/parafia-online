class CreateAttendanceListsUsers < ActiveRecord::Migration
  def change
    create_table :attendance_lists_users do |t|
      t.integer :attendance_list_id
      t.integer :user_id
    end
  end
end
