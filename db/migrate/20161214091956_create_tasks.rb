class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.belongs_to :work_plan, index: true
      t.belongs_to :event, index: true

      t.timestamps null: false
    end
  end
end
