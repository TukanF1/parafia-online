class CreateEventCategories < ActiveRecord::Migration
	def change
		create_table :event_categories do |t|
			t.string :name
			t.belongs_to :event, index: true

			t.timestamps null: false
		end
		
		add_index :event_categories, :name, unique: true
	end
end
