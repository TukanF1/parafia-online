class AddRolesToSubgroups < ActiveRecord::Migration
  def change
    add_column :subgroups, :admin, :boolean
    add_column :subgroups, :moderator, :boolean
  end
end
