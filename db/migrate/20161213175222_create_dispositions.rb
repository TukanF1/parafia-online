class CreateDispositions < ActiveRecord::Migration
  def change
    create_table :dispositions do |t|
      t.integer :weekday
      t.string :hour
      t.belongs_to :user, index: true
      t.belongs_to :event_category, index: true

      t.timestamps null: false
    end
  end
end
