class RemoveEventIndexFromEventCategories < ActiveRecord::Migration
  def change
    remove_index :event_categories, :event_id
  end
end
