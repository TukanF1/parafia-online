class CreateWorkPlans < ActiveRecord::Migration
  def change
    create_table :work_plans do |t|
      t.belongs_to :user, index: true
      t.belongs_to :main_group, index: true

      t.timestamps null: false
    end
  end
end
