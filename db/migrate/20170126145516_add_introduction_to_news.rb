class AddIntroductionToNews < ActiveRecord::Migration
  def change
    add_column :news, :introduction, :string
  end
end
