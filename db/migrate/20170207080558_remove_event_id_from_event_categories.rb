class RemoveEventIdFromEventCategories < ActiveRecord::Migration
  def change
    remove_column :event_categories, :event_id
  end
end
