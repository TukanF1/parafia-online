class AddScheduleTypeToEventCategories < ActiveRecord::Migration
  def change
    add_column :event_categories, :schedule_type, :string
  end
end
