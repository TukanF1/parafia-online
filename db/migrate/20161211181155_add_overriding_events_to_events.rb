class AddOverridingEventsToEvents < ActiveRecord::Migration
  def change
    add_reference :events, :overriding_event, index: true
  end
end
