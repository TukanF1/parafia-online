class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.text :content
      t.string :title
      t.belongs_to :main_group, index: true
      t.belongs_to :user, :author, index: true

      t.timestamps null: false
    end
  end
end
