class CreateSubgroups < ActiveRecord::Migration
  def change
    create_table :subgroups do |t|
      t.string :name, null: false
      t.belongs_to :main_group, index: true

      t.timestamps null: false
    end
  end
end
