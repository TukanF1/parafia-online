class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content
      t.belongs_to :news, index: true
      t.belongs_to :user, :author, index: true

      t.timestamps null: false
    end
  end
end
