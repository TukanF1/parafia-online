class CreateMainGroups < ActiveRecord::Migration
  def change
    create_table :main_groups do |t|
      t.string :name, null: false

      t.timestamps null: false
    end
    
    add_index :main_groups, :name, unique: true
  end
end
