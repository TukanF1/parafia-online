class RemoveUserIndexFromNews < ActiveRecord::Migration
  def change
    remove_index :news, :user_id
  end
end
