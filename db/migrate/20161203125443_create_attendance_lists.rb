class CreateAttendanceLists < ActiveRecord::Migration
  def change
    create_table :attendance_lists do |t|
      t.belongs_to :event, index: true

      t.timestamps null: false
    end
  end
end
