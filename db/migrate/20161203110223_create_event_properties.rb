class CreateEventProperties < ActiveRecord::Migration
  def change
    create_table :event_properties do |t|
      t.boolean :organizer
      t.integer :mandatory_presence
      t.belongs_to :event, index: true
      t.belongs_to :main_group, index: true

      t.timestamps null: false
    end
  end
end
