class CreateMandatoryAttendanceLists < ActiveRecord::Migration
  def change
    create_table :mandatory_attendance_lists do |t|
      t.belongs_to :event, index: true

      t.timestamps null: false
    end
  end
end
