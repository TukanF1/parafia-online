class CreateSubgroupsUsers < ActiveRecord::Migration
  def change
    create_table :subgroups_users do |t|
      t.integer :subgroup_id
      t.integer :user_id
    end
  end
end
