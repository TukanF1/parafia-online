class CreateMandatoryAttendanceListsUsers < ActiveRecord::Migration
	def change
		create_table :mandatory_attendance_lists_users do |t|
			t.integer :mandatory_attendance_list_id
			t.integer :user_id
		end
	end
end
