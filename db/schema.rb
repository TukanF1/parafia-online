# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170207080558) do

  create_table "attendance_lists", force: :cascade do |t|
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "attendance_lists", ["event_id"], name: "index_attendance_lists_on_event_id"

  create_table "attendance_lists_users", force: :cascade do |t|
    t.integer "attendance_list_id"
    t.integer "user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text     "content"
    t.integer  "news_id"
    t.integer  "user_id"
    t.integer  "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["author_id"], name: "index_comments_on_author_id"
  add_index "comments", ["news_id"], name: "index_comments_on_news_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  create_table "dispositions", force: :cascade do |t|
    t.integer  "weekday"
    t.string   "hour"
    t.integer  "user_id"
    t.integer  "event_category_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "dispositions", ["event_category_id"], name: "index_dispositions_on_event_category_id"
  add_index "dispositions", ["user_id"], name: "index_dispositions_on_user_id"

  create_table "event_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "schedule_type"
  end

  add_index "event_categories", ["name"], name: "index_event_categories_on_name", unique: true

  create_table "event_properties", force: :cascade do |t|
    t.boolean  "organizer"
    t.integer  "mandatory_presence"
    t.integer  "event_id"
    t.integer  "main_group_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "event_properties", ["event_id"], name: "index_event_properties_on_event_id"
  add_index "event_properties", ["main_group_id"], name: "index_event_properties_on_main_group_id"

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "description"
    t.string   "color"
    t.integer  "event_category_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "overriding_event_id"
    t.integer  "previous_in_series_event_id"
  end

  add_index "events", ["event_category_id"], name: "index_events_on_event_category_id"
  add_index "events", ["overriding_event_id"], name: "index_events_on_overriding_event_id"
  add_index "events", ["previous_in_series_event_id"], name: "index_events_on_previous_in_series_event_id"

  create_table "main_groups", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "main_groups", ["name"], name: "index_main_groups_on_name", unique: true

  create_table "mandatory_attendance_lists", force: :cascade do |t|
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "mandatory_attendance_lists", ["event_id"], name: "index_mandatory_attendance_lists_on_event_id"

  create_table "mandatory_attendance_lists_users", force: :cascade do |t|
    t.integer "mandatory_attendance_list_id"
    t.integer "user_id"
  end

  create_table "news", force: :cascade do |t|
    t.text     "content"
    t.string   "title"
    t.integer  "main_group_id"
    t.integer  "user_id"
    t.integer  "author_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "introduction"
  end

  add_index "news", ["author_id"], name: "index_news_on_author_id"
  add_index "news", ["main_group_id"], name: "index_news_on_main_group_id"

  create_table "points", force: :cascade do |t|
    t.integer  "points"
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "points", ["event_id"], name: "index_points_on_event_id"
  add_index "points", ["user_id"], name: "index_points_on_user_id"

  create_table "subgroups", force: :cascade do |t|
    t.string   "name",          null: false
    t.integer  "main_group_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "admin"
    t.boolean  "moderator"
  end

  add_index "subgroups", ["main_group_id"], name: "index_subgroups_on_main_group_id"

  create_table "subgroups_users", force: :cascade do |t|
    t.integer "subgroup_id"
    t.integer "user_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.integer  "work_plan_id"
    t.integer  "event_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "tasks", ["event_id"], name: "index_tasks_on_event_id"
  add_index "tasks", ["work_plan_id"], name: "index_tasks_on_work_plan_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",       null: false
    t.string   "encrypted_password",     default: "",       null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,        null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,        null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "first_name",             default: "Gall",   null: false
    t.string   "second_name"
    t.string   "last_name",              default: "Anonim", null: false
    t.datetime "date_of_birth"
    t.boolean  "is_female",              default: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true

  create_table "work_plans", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "main_group_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "work_plans", ["main_group_id"], name: "index_work_plans_on_main_group_id"
  add_index "work_plans", ["user_id"], name: "index_work_plans_on_user_id"

end
